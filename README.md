# prepare_wgs_data

Check WGS data

# 0. Downloads data

see details @ [mullus_surmuletus/infos](mullus_surmuletus/infos)

# 1. Check quality

## 1.1 Fastqc

We run fastqc on each fastq files. Reports are stored into [mullus_surmuletus/fastqc_report/](mullus_surmuletus/fastqc_report/)

```
nohup bash mullus_surmuletus/control_quality.sh &
```

## 1.2 MultiQC

MultiQC searches a given directory for analysis logs and compiles a HTML report. It's a general use tool, perfect for summarising the output of fastqc

```
RUN_FOLDER=`pwd`"/mullus_surmuletus/fastqc_report/"
docker run -v ${RUN_FOLDER}:${RUN_FOLDER} -w ${RUN_FOLDER} ewels/multiqc .
```

