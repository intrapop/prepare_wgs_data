## perform a fastqc

## singularity container path with fastqc
SINGULARITY_IMG="/media/superdisk/utils/conteneurs/recettes/fastqc.simg"
## folder with fastq files path
RUN_FOLDER="/media/superdisk/popedna/donnees/ngs/mullus_surmuletus/"

## run fastqc on each fastq file
for FICHIER in $(ls ${RUN_FOLDER})
do
    SAMPLE=`basename ${FICHIER}`
    singularity run --bind /media/superdisk:/media/superdisk/ $SINGULARITY_IMG ${RUN_FOLDER}/${SAMPLE} -o mullus_surmuletus/fastqc_report/
done


## cluster fastqc report
#RUN_FOLDER=`pwd`"/mullus_surmuletus/fastqc_report/"
#docker run -v ${RUN_FOLDER}:${RUN_FOLDER} -w ${RUN_FOLDER} ewels/multiqc .